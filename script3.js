function filterBy(array, dataType) {
   
    return array.filter(item => typeof item !== dataType);
}

let data = ['hello', 'world', 23, '23', null];
let filteredData = filterBy(data, 'string');

console.log(filteredData)