



//------------------------------ 2 ---------------------

let peopleArray = [
    { name: "Іван", age: 25, sex: "чоловіча" },
    { name: "Марія", age: 30, sex: "жіноча" },
    { name: "Петро", age: 22, sex: "чоловіча" },
    { name: "Анна", age: 28, sex: "жіноча" }
] ;
 
let people = peopleArray.filter(person => person.sex === "чоловіча");
console.log(people);